package com.gramirez.adminusuarios;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.gramirez.adminusuarios.Model.Cliente;
import com.gramirez.adminusuarios.Model.Direccion;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private ArrayList<Cliente> clientes;
    private ListView listViewClientes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        llenarDatosPrueba();

        listViewClientes = findViewById(R.id.listViewClientes);
        listViewClientes.setOnItemClickListener(this);

        presentarDatos();
    }

    public void llenarDatosPrueba(){
        clientes = new ArrayList<>();
        ArrayList<Direccion> direcciones = new ArrayList<>();
        direcciones.add(new Direccion(1, "Calle La Rosa #13, El millón, Santo Domingo Oeste"));

        clientes.add(new Cliente(1, "Juan Sosa", direcciones));
        clientes.add(new Cliente(1, "María Sosa", direcciones));
        clientes.add(new Cliente(1, "Pedro Ramírez", direcciones));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == R.id.agregar_cliente) {
            Intent intent = new Intent(MainActivity.this, AgregarClienteActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    private void presentarDatos() {
        ArrayAdapter<Cliente> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, clientes);

        listViewClientes.setAdapter(adapter);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
         // Agregar funcion de ir a activity Detalle de cliente
    }
}