package com.gramirez.adminusuarios.Model;

import java.util.ArrayList;

public class Cliente {

    private int id;
    private String nombre;
    private ArrayList<Direccion> direcciones;

    public Cliente(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public Cliente(int id, String nombre, ArrayList<Direccion> direcciones) {
        this.id = id;
        this.nombre = nombre;
        this.direcciones = direcciones;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ArrayList<Direccion> getDirecciones() {
        return direcciones;
    }

    public void setDirecciones(ArrayList<Direccion> direcciones) {
        this.direcciones = direcciones;
    }

    @Override
    public String toString() {
        return nombre;
    }
}
